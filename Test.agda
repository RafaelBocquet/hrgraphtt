{-# OPTIONS --rewriting --type-in-type --confluence-check #-}

module Test where

open import Agda.Primitive renaming (Set to Type)

infixr 50 _,_

record ⊤ : Type where
  constructor tt

record Σ (A : Type) (B : A -> Type) : Type where
  constructor _,_
  field fst : A
        snd : B fst
open Σ

data _≡_ {A : Type} (x : A) : A → Type where
  refl : x ≡ x
{-# BUILTIN EQUALITY _≡_ #-}
{-# BUILTIN REWRITE _≡_ #-}

postulate
  IsContr : Type → Type

  Id   : (A : Type) (x y : A) → Type
  IdR  : (A : Type) (x : A) (p : Id A x x) → Type
  DId  : (A : Type) (B : A → Type) (a₁ a₂ : A) → Id A a₁ a₂
       → B a₁ → B a₂ → Type
  DIdR : (A : Type) (B : A → Type) (a : A) (p : Id A a a) → IdR A a p
       → (b : B a) → DId A B a a p b b → Type

  center : ∀ {A} → IsContr A → A
  path   : ∀ {A} → IsContr A → ∀ x y → Id A x y
  pathR  : ∀ {A} → (c : IsContr A) → ∀ x → IdR A x (path c x x)
  dpath  : ∀ {A} {B : A → Type} → (∀ a → IsContr (B a))
           → ∀ a₁ a₂ p b₁ b₂ → DId A B a₁ a₂ p b₁ b₂
  dpathR : ∀ {A} {B : A → Type} → (c : ∀ a → IsContr (B a))
           → ∀ a p r b → DIdR A B a p r b (dpath c a a p b b)

  Id-l  : ∀ A a → IsContr (Σ A λ b → Id A a b)
  Id-r  : ∀ A b → IsContr (Σ A λ a → Id A a b)
  IdR-c : ∀ A a → IsContr (Σ (Id A a a) λ p → IdR A a p)

  DId-l  : ∀ A B a₁ a₂ p x → IsContr (Σ (B a₂) λ y → DId A B a₁ a₂ p x y)
  DId-r  : ∀ A B a₁ a₂ p y → IsContr (Σ (B a₁) λ x → DId A B a₁ a₂ p x y)
  DIdR-c : ∀ A B a p r x → IsContr (Σ _ λ q → DIdR A B a p r x q)

postulate
  IsContr-Σ⊤ : IsContr (Σ ⊤ λ _ → ⊤)

  IsContr-ΣΣ : ∀ {A : Type} {B : A → Type} {C : A → Type} {D : ∀ a → B a → C a → Type}
             → IsContr (Σ A C)
             → (∀ a c → IsContr (Σ (B a) λ b → D a b c))
             → IsContr (Σ (Σ A B) λ p → Σ (C (p .fst)) λ c → D (p .fst) (p .snd) c)

-- Top
postulate
  Id-⊤ : Id ⊤ ≡ λ _ _ → ⊤
{-# REWRITE Id-⊤ #-}
postulate
  IdR-⊤ : IdR ⊤ ≡ λ _ _ → ⊤
{-# REWRITE IdR-⊤ #-}
postulate
  DId-⊤ : ∀ A → DId A (λ _ → ⊤) ≡ λ _ _ _ _ _ → ⊤
{-# REWRITE DId-⊤ #-}
postulate
  DIdR-⊤ : ∀ A → DIdR A (λ _ → ⊤) ≡ λ _ _ _ _ _ → ⊤
{-# REWRITE DIdR-⊤ #-}
postulate
  Id-l-⊤ : Id-l ⊤ ≡ λ a → IsContr-Σ⊤
  Id-r-⊤ : Id-r ⊤ ≡ λ a → IsContr-Σ⊤
  IdR-c-⊤ : IdR-c ⊤ ≡ λ a → IsContr-Σ⊤
{-# REWRITE Id-l-⊤ Id-r-⊤ IdR-c-⊤ #-}
postulate
  DId-l-⊤ : ∀ A → DId-l A (λ _ → ⊤) ≡ λ _ _ _ _ → IsContr-Σ⊤
  DId-r-⊤ : ∀ A → DId-l A (λ _ → ⊤) ≡ λ _ _ _ _ → IsContr-Σ⊤
  DIdR-c-⊤ : ∀ A → DIdR-c A (λ _ → ⊤) ≡ λ _ _ _ _ → IsContr-Σ⊤
{-# REWRITE DId-l-⊤ DId-r-⊤ DIdR-c-⊤ #-}

-- Sigma
postulate
  Id-Σ : ∀ A B → Id (Σ A B) ≡ λ p q → Σ (Id A (p .fst) (q .fst)) λ e → DId A B _ _ e (p .snd) (q .snd)
{-# REWRITE Id-Σ #-}
postulate
  IdR-Σ : ∀ A B → IdR (Σ A B) ≡ λ a p → Σ (IdR A (a .fst) (p .fst)) λ e → DIdR A B _ _ e (a .snd) (p .snd)
{-# REWRITE IdR-Σ #-}
postulate
  DId-Σ : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
        → DId A (λ a → Σ (B a) (C a))
        ≡ λ a b e p q →
          Σ (DId A B a b e (p .fst) (q .fst)) λ f →
             DId (Σ A B) (λ p → C (p .fst) (p .snd)) (a , p .fst) (b , q .fst) (e , f) (p .snd) (q .snd)
{-# REWRITE DId-Σ #-}
postulate
  DIdR-Σ : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
         → DIdR A (λ a → Σ (B a) (C a))
         ≡ λ a p r x α →
           Σ (DIdR A B a p r (x .fst) (α .fst)) λ f →
              DIdR (Σ A B) (λ p → C (p .fst) (p .snd)) (a , x .fst) (p , (α .fst)) (r , f) (x .snd) (α .snd)
{-# REWRITE DIdR-Σ #-}
postulate
  Id-l-Σ : ∀ A B → Id-l (Σ A B) ≡ λ a → IsContr-ΣΣ (Id-l A (a .fst)) (λ b p → DId-l A B (a .fst) b p (a .snd))
  Id-r-Σ : ∀ A B → Id-r (Σ A B) ≡ λ a → IsContr-ΣΣ (Id-r A (a .fst)) (λ b p → DId-r A B b (a .fst) p (a .snd))
  IdR-c-Σ : ∀ A B → IdR-c (Σ A B) ≡ λ a → IsContr-ΣΣ (IdR-c A (a .fst)) (λ p r → DIdR-c A B (a .fst) p r (a .snd))
{-# REWRITE Id-l-Σ Id-r-Σ IdR-c-Σ #-}
postulate
  DId-l-Σ : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
            → DId-l A (λ a → Σ (B a) (C a))
            ≡ λ _ _ _ _ → IsContr-ΣΣ (DId-l A B _ _ _ _) λ _ _ →
                                      DId-l (Σ A B) (λ p → C (p .fst) (p .snd)) _ _ _ _
  DId-r-Σ : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
            → DId-r A (λ a → Σ (B a) (C a))
            ≡ λ _ _ _ _ → IsContr-ΣΣ (DId-r A B _ _ _ _) λ _ _ →
                                      DId-r (Σ A B) (λ p → C (p .fst) (p .snd)) _ _ _ _
  DIdR-c-Σ : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
             → DIdR-c A (λ a → Σ (B a) (C a))
             ≡ λ a p r x → IsContr-ΣΣ (DIdR-c A B _ _ _ _) λ _ _ →
                                       DIdR-c (Σ A B) (λ p → C (p .fst) (p .snd)) _ _ _ _
{-# REWRITE DId-l-Σ DId-r-Σ DIdR-c-Σ #-}

-- Pi
-- postulate
--   Id-Π : ∀ A B → Id ((a : A) → B a) ≡ λ f₁ f₂ → ∀ a₁ a₂ → (p : Id A a₁ a₂) → DId A B a₁ a₂ p (f₁ a₁) (f₂ a₂)
-- {-# REWRITE Id-Π #-}
-- postulate
--   IdR-Π : ∀ A B → IdR ((a : A) → B a) ≡ λ f p → ∀ a q (r : IdR A a q) → DIdR A B a q r (f a) (p _ _ q)
-- {-# REWRITE IdR-Π #-}
postulate
  DId-Π : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
        → DId A (λ a → (b : B a) → C a b)
        ≡ λ a₁ a₂ p f₁ f₂ → ∀ b₁ b₂ q → DId (Σ A B) (λ p → C (p .fst) (p .snd)) (a₁ , b₁) (a₂ , b₂) (p , q) (f₁ b₁) (f₂ b₂)
{-# REWRITE DId-Π #-}
postulate
  DIdR-Π : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
         → DIdR A (λ a → (b : B a) → C a b)
         ≡ λ a p r f α → ∀ b q t → DIdR (Σ A B) (λ p → C (p .fst) (p .snd)) (a , b) (p , q) (r , t) (f b) (α _ _ q)
{-# REWRITE DIdR-Π #-}

-- postulate
--   IsContr-l-Π : ∀ {A : Type} {B : A → Type} (f₁ : (a : A) → B a)
--               → IsContr (Σ _ λ f₂ → Id ((a : A) → B a) f₁ f₂)
--   IsContr-r-Π : ∀ {A : Type} {B : A → Type} (f₂ : (a : A) → B a)
--               → IsContr (Σ _ λ f₁ → Id ((a : A) → B a) f₁ f₂)
--   IsContr-c-Π : ∀ {A : Type} {B : A → Type} (f : (a : A) → B a)
--               → IsContr (Σ _ λ g → IdR ((a : A) → B a) f g)

-- postulate
--   Id-l-Π : ∀ A (B : A → Type) → Id-l ((a : A) → B a) ≡ IsContr-l-Π
--   Id-r-Π : ∀ A (B : A → Type) → Id-r ((a : A) → B a) ≡ IsContr-r-Π
--   IdR-c-Π : ∀ A (B : A → Type) → IdR-c ((a : A) → B a) ≡ IsContr-c-Π
-- {-# REWRITE Id-l-Π Id-r-Π IdR-c-Π #-}

postulate
  IsContrD-l-Π : ∀ {A : Type} {B : A → Type} {C : (a : A) → B a → Type}
                  (a₁ a₂ : A) (p : Id A a₁ a₂) (f₁ : (b : B a₁) → C a₁ b)
              → IsContr (Σ _ λ f₂ → DId A (λ a → (b : B a) → C a b) _ _ p f₁ f₂)
  IsContrD-r-Π : ∀ {A : Type} {B : A → Type} {C : (a : A) → B a → Type}
                  (a₁ a₂ : A) (p : Id A a₁ a₂) (f₂ : (b : B a₂) → C a₂ b)
              → IsContr (Σ _ λ f₁ → DId A (λ a → (b : B a) → C a b) _ _ p f₁ f₂)
  IsContrD-c-Π : ∀ {A : Type} {B : A → Type} {C : (a : A) → B a → Type}
                  a p r f
              → IsContr (Σ _ λ g → DIdR A (λ a → (b : B a) → C a b) a p r f g)

postulate
  DId-l-Π : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
          → DId-l A (λ a → (b : B a) → C a b) ≡ IsContrD-l-Π
  DId-r-Π : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
          → DId-r A (λ a → (b : B a) → C a b) ≡ IsContrD-r-Π
  DIdR-c-Π : ∀ A (B : A → Type) (C : ∀ a → B a → Type)
           → DIdR-c A (λ a → (b : B a) → C a b) ≡ IsContrD-c-Π
{-# REWRITE DId-l-Π DId-r-Π DIdR-c-Π #-}


-- Type
Equiv : Type → Type → Type
Equiv A B = Σ (A → B → Type) λ E →
            Σ (∀ a → IsContr (Σ _ λ b → E a b)) λ _ →
              (∀ b → IsContr (Σ _ λ a → E a b))

IsRefl : ∀ A → Equiv A A → Type
IsRefl A E = Σ (∀ a → E .fst a a → Type) λ R →
               (∀ a → IsContr (Σ _ λ p → R a p))

postulate
  IsContr-l-Equiv : ∀ A → IsContr (Σ Type λ B → Equiv A B)
  IsContr-r-Equiv : ∀ B → IsContr (Σ Type λ A → Equiv A B)
  IsContr-c-Equiv : ∀ A → IsContr (Σ (Equiv A A) λ E → IsRefl _ E)

-- postulate
--   Id-Type  : Id Type ≡ Equiv
-- {-# REWRITE Id-Type #-}
-- postulate
--   IdR-Type : IdR Type ≡ IsRefl
-- {-# REWRITE IdR-Type #-}
postulate
  DId-Type : ∀ A → DId A (λ _ → Type) ≡ λ _ _ _ → Equiv
{-# REWRITE DId-Type #-}
postulate
  DIdR-Type : ∀ A → DIdR A (λ _ → Type) ≡ λ _ _ _ → IsRefl
{-# REWRITE DIdR-Type #-}
-- postulate
--   Id-l-Type : Id-l Type ≡ IsContr-l-Equiv
--   Id-r-Type : Id-r Type ≡ IsContr-r-Equiv
--   IdR-c-Type : IdR-c Type ≡ IsContr-c-Equiv
-- {-# REWRITE Id-l-Type Id-r-Type IdR-c-Type #-}
postulate
  DId-l-Type : ∀ A → DId-l A (λ _ → Type) ≡ λ _ _ _ → IsContr-l-Equiv
  DId-r-Type : ∀ A → DId-r A (λ _ → Type) ≡ λ _ _ _ → IsContr-r-Equiv
  DIdR-c-Type : ∀ A → DIdR-c A (λ _ → Type) ≡ λ _ _ _ → IsContr-c-Equiv
{-# REWRITE DId-l-Type DId-r-Type DIdR-c-Type #-}

postulate
  DId-str : ∀ A (B : A → Type) (C : A → Type) {a₁ a₂ p c₁ c₂}
          → DId (Σ A B) (λ p → C (p .fst)) a₁ a₂ p c₁ c₂
          → DId A C (a₁ .fst) (a₂ .fst) (p .fst) c₁ c₂
  DId-wk : ∀ A (B : A → Type) (C : A → Type) {a₁ a₂ p c₁ c₂}
         → DId A C (a₁ .fst) (a₂ .fst) (p .fst) c₁ c₂
         → DId (Σ A B) (λ p → C (p .fst)) a₁ a₂ p c₁ c₂
  DId-str-wk : ∀ A (B : A → Type) (C : A → Type) {a₁ a₂ p c₁ c₂}
             → ∀ x → DId-str A B C (DId-wk A B C {a₁} {a₂} {p} {c₁} {c₂} x) ≡ x
  DId-wk-str : ∀ A (B : A → Type) (C : A → Type) {a₁ a₂ p c₁ c₂}
             → ∀ x → DId-wk A B C (DId-str A B C {a₁} {a₂} {p} {c₁} {c₂} x) ≡ x
{-# REWRITE DId-str-wk DId-wk-str #-}
postulate
  DId-swap : ∀ A (B : A → Type) (C : A → Type) (D : ∀ a → B a → C a → Type)
               {a₁ a₂ b₁ b₂ c₁ c₂ pa pb pc d₁ d₂}
         → DId (Σ (Σ A B) (λ p → C (p .fst))) (λ p → D (p .fst .fst) (p .fst .snd) (p .snd))
               ((a₁ , b₁) , c₁) ((a₂ , b₂) , c₂) ((pa , pb) , pc) d₁ d₂
         → DId (Σ (Σ A C) (λ p → B (p .fst))) (λ p → D (p .fst .fst) (p .snd) (p .fst .snd))
               ((a₁ , c₁) , b₁) ((a₂ , c₂) , b₂) ((pa , DId-str A B C pc) , DId-wk A C B pb) d₁ d₂
  DId-swap-swap : ∀ A (B : A → Type) (C : A → Type) (D : ∀ a → B a → C a → Type)
                    {a₁ a₂ b₁ b₂ c₁ c₂ pa pb pc d₁ d₂}
                → ∀ x → DId-swap A B C D {a₁} {a₂} {b₁} {b₂} {c₁} {c₂} {pa} {pb} {pc} {d₁} {d₂} (DId-swap A C B (λ a c b → D a b c) x) ≡ x
-- {-# REWRITE DId-swap-swap #-}


postulate
  center-IsContr-Σ⊤ : center IsContr-Σ⊤ ≡ _
{-# REWRITE center-IsContr-Σ⊤ #-}
postulate
  path-IsContr-Σ⊤ : path IsContr-Σ⊤ ≡ _
{-# REWRITE path-IsContr-Σ⊤ #-}
postulate
  pathR-IsContr-Σ⊤ : pathR IsContr-Σ⊤ ≡ _
{-# REWRITE pathR-IsContr-Σ⊤ #-}
postulate
  dpath-IsContr-Σ⊤ : ∀ {A} → dpath {A} (λ _ → IsContr-Σ⊤) ≡ _
{-# REWRITE dpath-IsContr-Σ⊤ #-}
postulate
  dpathR-IsContr-Σ⊤ : ∀ {A} → dpathR {A} (λ _ → IsContr-Σ⊤) ≡ _
{-# REWRITE dpathR-IsContr-Σ⊤ #-}

postulate
  center-IsContr-ΣΣ : ∀ {A B C D} c d
                      → center (IsContr-ΣΣ {A} {B} {C} {D} c d)
                      ≡ let cc = center c
                            cd = center (d (cc .fst) (cc .snd))
                        in ((cc .fst , cd .fst) , (cc .snd , cd .snd))
{-# REWRITE center-IsContr-ΣΣ #-}

postulate
  path-IsContr-ΣΣ : ∀ {A B C D} c d
                    → path (IsContr-ΣΣ {A} {B} {C} {D} c d)
                    ≡ λ { ((a₁ , b₁) , (c₁ , d₁)) ((a₂ , b₂) , (c₂ , d₂))
                        → let pc = path c (a₁ , c₁) (a₂ , c₂)
                              cd = dpath {Σ A C} (λ p → d (p .fst) (p .snd))
                                         _ _ pc (b₁ , d₁) (b₂ , d₂)
                          in ( (pc .fst , DId-str A C B (cd .fst)))
                              , (DId-wk A B C (pc .snd) , DId-swap A C B (λ a c b → D a b c) (cd .snd))
                        }
{-# REWRITE path-IsContr-ΣΣ #-}

postulate
  pathR-IsContr-ΣΣ : ∀ {A B C D} x y
                     → pathR (IsContr-ΣΣ {A} {B} {C} {D} x y)
                     ≡ λ { ((a , b) , (c , d))
                         → let px = pathR x (a , c)
                               py = dpathR {Σ A C} (λ p → y (p .fst) (p .snd))
                                           _ _ px (b , d)
                           in (px .fst , {!py .fst!}) , ({!!} , {!!})
                         }

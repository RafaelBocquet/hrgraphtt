open Common

type ctx
  = { tctx : Top.ctx
    }

let create_ctx ()
  = { tctx = Top.create_ctx ();
    }

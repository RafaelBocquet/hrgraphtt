open Common

type ctx = Value.eval_ctx

let empty : ctx
  = { lvl   = 0;
      env   = EEmpty;
      types = EEmpty;
    }

let bind (ctx : ctx) ty : ctx
  = { lvl   = ctx.lvl+1;
      env   = ESnoc(ctx.env, Value.var ctx.lvl);
      types = ESnoc(ctx.types, ty);
    }

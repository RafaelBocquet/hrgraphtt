type idx = Idx of int
[@@unboxed]
[@@deriving show]

type lvl = int
[@@deriving show]

type top = Top of int
[@@unboxed]
[@@deriving show]

module IntSet = Set.Make(Int)
module IntMap = Map.Make(Int)
module NameMap = Map.Make(String)

let lvl_to_idx (sz : lvl) (lvl : lvl) : idx
  = Idx (sz-1-lvl)

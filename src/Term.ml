open Common

type t
  = Var of idx
  | Top of top
  | Let of t * t
  | Type
  (* Pi *)
  | Pi of t * t
  | Lam of t * t
  | App of t * t * t
  (* Sigma *)
  | Sigma of t * t
  | Pair of t * t
  | Fst of t
  | Snd of t
  (* Unit *)
  | Unit
  | UnitTT
  (* Bottom *)
  | Bottom
  | Absurd of t * t
  (* Bool *)
  | Bool
  | True
  | False
  | ITE of t * t*t * t
  (* Ap *)
  | Ap0 of tele * t*t
  | Ap of tele * t*t * t*t*t
[@@deriving show]

and tele = TeleEmpty
         | TeleExtend of tele * t
[@@deriving show]

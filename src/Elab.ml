open Common

module Ctx = ElabCtx
module LCtx = EvalCtx

let rec check (ctx : Ctx.ctx) (ty : Value.t) (rtm : Raw.tm) : Term.t =
  check_ ctx (rtm,ty)

and check_ ctx = function
  | (Lam((x,None),rt), Pi(a,b)) ->
    let t = check (Ctx.bind ctx x a)
        (Eval.clAppVar ctx.gctx (LCtx.bind ctx.lctx a) b ctx.lctx.lvl) rt in
    Term.Lam(Ctx.quote ctx a, t)

  | (Pair(rx,ry), Sigma(a,b)) ->
    let x = check ctx a rx in
    let vx = Ctx.eval ctx x in
    let y = check ctx (Eval.clApp ctx.gctx ctx.lctx b vx) ry in
    Term.Pair(x,y)

  | (Let(x,Some(ra),rt,ru),ty) ->
    let a  = check ctx Type ra in
    let va = Ctx.eval ctx a in
    let t  = check ctx va rt in
    let vt = Ctx.eval ctx t in
    let u  = check (Ctx.define ctx (Some(x)) va vt) ty ru in
    Term.Let(t,u)

  | (Let(x,None,rt,ru),ty) ->
    let t,va = infer ctx rt in
    let vt   = Ctx.eval ctx t in
    let u    = check (Ctx.define ctx (Some(x)) va vt) ty ru in
    Term.Let(t,u)

  | rtm,ty -> check_default ctx ty rtm

and check_default ctx ty rtm =
  let tm, ty' = infer ctx rtm in
  Eval.conv ctx.gctx ctx.lctx Type ty ty';
  tm

and check_type ctx rtm =
  check_default ctx Type rtm

and check_tele ctx : Raw.tele -> Term.tele = function
  | TeleEmpty ->
    TeleEmpty
  | TeleExtend(rtele, _, ra) ->
    let tele  = check_tele ctx rtele in
    let vtele = Ctx.eval_tele ctx tele in
    let a     = check_type (Ctx.bind_tele ctx rtele vtele) ra in
    TeleExtend(tele, a)

and infer (ctx : Ctx.ctx) : Raw.tm -> Term.t * Value.t = function
  | Var(x)
    -> Ctx.lookup ctx x

  | Type ->
    Term.Type, Value.Type

  | Let(x,Some(ra),rt,ru) ->
    let a  = check ctx Type ra in
    let va = Ctx.eval ctx a in
    let t  = check ctx va rt in
    let vt = Ctx.eval ctx t in
    let u, ty = infer (Ctx.define ctx (Some(x)) va vt) ru in
    Term.Let(t,u),
    ty

  | Let(x,None,rt,ru) ->
    let t, va = infer ctx rt in
    let vt = Ctx.eval ctx t in
    let u, ty = infer (Ctx.define ctx (Some(x)) va vt) ru in
    Term.Let(t,u),
    ty

  (* Pi *)
  | Pi((x,Some(ra)),rb) ->
    let a  = check_type ctx ra in
    let va = Ctx.eval ctx a in
    let b  = check_type (Ctx.bind ctx x va) rb in
    Term.Pi(a,b),
    Type

  | Lam((x,Some(ra)),rt) ->
    let a     = check_type ctx ra in
    let va    = Ctx.eval ctx a in
    let ctxb  = Ctx.bind ctx x va in
    let t, vb = infer ctxb rt in
    let b     = Ctx.quote ctxb vb in
    Term.Lam(a, t),
    Value.Pi(va, Cl(ctx.env, b))

  | App(rf,ru) ->
    let f, ty = infer ctx rf in
    let va, b = match ty with
      | Pi(va,b) -> va,b
      | _ -> failwith "FAIL"
    in
    let u = check ctx va ru in
    let a = Ctx.quote ctx va in
    Term.App(f,a,u),
    Eval.clApp ctx.gctx ctx.lctx b (Ctx.eval ctx u)

  (* Sigma *)
  | Sigma(x,ra,rb) ->
    let a  = check_type ctx ra in
    let va = Ctx.eval ctx a in
    let b  = check_type (Ctx.bind ctx x va) rb in
    Term.Sigma(a, b), Value.Type

  | Fst(rp) ->
    let p, ty = infer ctx rp in
    let va, _ = match ty with
      | Sigma(va,b) -> va, b
      | _ -> failwith "FAIL"
    in
    Term.Fst(p), va

  | Snd(rp) ->
    let p, ty = infer ctx rp in
    let _, b = match ty with
      | Sigma(va,b) -> va, b
      | _ -> failwith "FAIL"
    in
    Term.Snd(p),
    Eval.clApp ctx.gctx ctx.lctx b (Ctx.eval ctx (Term.Fst(p)))

  (* Unit *)
  | Unit ->
    Term.Unit, Value.Type

  | UnitTT ->
    Term.UnitTT, Value.Unit

  (* Bottom *)
  | Bottom ->
    Term.Bottom, Value.Type

  | Absurd ->
    Term.(Lam(Pi(Bottom,Type), Lam(Bottom, Absurd(Var(Idx(1)),Var(Idx(0)))))),
    Value.(
      Pi(Pi(Bottom, ClConst(Type)), ClFun(fun lvl f ->
        Pi(Bottom, ClFun(fun lvl x ->
              Eval.app ctx.gctx lvl f Bottom x
            ))))
    )

  (* Bool *)
  | Bool ->
    Term.Bool, Value.Type

  | True ->
    Term.True, Value.Bool

  | False ->
    Term.False, Value.Bool

  | ITE ->
    Term.(Lam(Pi(Bool,Type),
              Lam(App(Var(Idx(0)), Bool, True),
                  Lam(App(Var(Idx(1)), Bool, False),
                    Lam(Bool,
                      ITE(App(Var(Idx(4)),Bool,Var(Idx(0))),Var(Idx(2)),Var(Idx(1)),Var(Idx(0)))))))),
    Value.(
      Pi(Pi(Bool, ClConst(Type)), ClFun(fun lctx f ->
          Pi(Eval.app ctx.gctx lctx f Bool True, ClConst(
              Pi(Eval.app ctx.gctx lctx f Bool False, ClConst(
                  Pi(Bool, ClFun(fun lctx b ->
                      Eval.app ctx.gctx lctx f Bool b
                    )))))))))

  (* Ap *)
  | Ap(rtele,rty,rtm) ->
    let tele  = check_tele ctx rtele in
    let vtele = Ctx.eval_tele ctx tele in
    let ty    = check_type (Ctx.bind_tele ctx rtele vtele) rty in
    let vty   = Ctx.eval (Ctx.bind_tele ctx rtele vtele) ty in
    let tm    = check (Ctx.bind_tele ctx rtele vtele) vty rtm in
    let clty     = Value.Cl(ctx.env, ty) in
    let cltm     = Value.Cl(ctx.env, tm) in
    let ftm lctx = Eval.clApp ctx.gctx lctx cltm in
    let tele_ty  = Eval.tele_type vtele in
    Ap0(tele,ty,tm),
    Value.(
      Pi(tele_ty, ClFun(fun lctx x0 ->
          Pi(tele_ty, ClFun(fun lctx x1 ->
              Pi(Eval.id_tele ctx.gctx lctx vtele x0 x1, ClFun(fun lctx x2 ->
                  Eval.id ctx.gctx lctx vtele clty x0 x1 x2 (ftm lctx x0) (ftm lctx x1)
                )))))))

  | v ->
    Format.fprintf Format.std_formatter "TODO: %a\n" Raw.pp_tm v;
    failwith __LOC__

let elab_top (gctx : Global.ctx) globals : Raw.top -> unit = function
  | Def(x_opt,rty_opt,rtm) ->
    let ctx  = Ctx.empty gctx !globals in
    let lctx = LCtx.empty in
    let ty,vty,tm,vtm = match rty_opt with
      | Some(rty) ->
        let ty   = check_type ctx rty in
        let vty  = Eval.eval gctx lctx EEmpty ty in
        let tm   = check ctx vty rtm in
        let vtm  = Eval.eval gctx lctx EEmpty tm in
        ty,vty,tm,vtm
      | None ->
        let tm, vty = infer ctx rtm in
        let vtm = Eval.eval gctx lctx EEmpty tm in
        let ty = Eval.quote gctx lctx vty in
        ty,vty,tm,vtm
    in
    let qtm = Eval.quote gctx lctx vtm in
    (* Format.fprintf Format.std_formatter "def %s =@.  %a@." *)
    (*   (Option.fold ~none:"_" ~some:(fun x -> x) x_opt) Term.pp qtm; *)
    assert(Eval.conv_bool gctx lctx vty vtm (Eval.eval gctx lctx EEmpty qtm));
    Option.iter (fun x ->
        let i    = Top.push gctx.tctx ty vty tm vtm x in
        globals := NameMap.add x (Ctx.Global(i)) !globals;
      ) x_opt;
    ()

let elab_file (f : Raw.file) =
  let gctx = Global.create_ctx () in
  let globals = ref NameMap.empty in
  List.iter (elab_top gctx globals) f

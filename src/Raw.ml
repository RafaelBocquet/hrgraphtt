open Common

type tm
  = Var of string
  | Let of string * tm option * tm * tm
  | Type
  (* Pi *)
  | Pi of binder * tm
  | Lam of binder * tm
  | App of tm * tm
  (* Sigma *)
  | Sigma of string option * tm * tm
  | Pair of tm * tm
  | Fst of tm
  | Snd of tm
  (* Unit *)
  | Unit
  | UnitTT
  (* Bottom *)
  | Bottom
  | Absurd
  (* Bool *)
  | Bool
  | True
  | False
  | ITE
  (* Ap *)
  | Ap of tele * tm * tm
[@@deriving show]

and binder = string option * tm option
[@@deriving show]

and tele = TeleEmpty
         | TeleExtend of tele * string option * tm
[@@deriving show]

type top
  = Def of string option * tm option * tm
[@@deriving show]

type file = top list
[@@deriving show]


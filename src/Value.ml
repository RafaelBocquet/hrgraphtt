open Common

type t
  = Var of lvl * refl_spine * spine
  | Type

  | Pi of t * tm_closure
  | Lam of t * tm_closure

  | Sigma of t * tm_closure
  | Pair of t * t

  | Unit
  | UnitTT

  | Bottom

  | Bool
  | True
  | False
[@@deriving show]

and env = EEmpty
        | ESnoc of env * t
[@@deriving show]

and refl_spine
  = RSEmpty
  | RSRefl of refl_spine

and spine
  = SEmpty
  | SApp of spine * (* type *) t * (* arg *) t
  | SFst of spine
  | SSnd of spine
  | SAbsurd of tm_closure * spine
  | SITE of tm_closure * t*t * spine
[@@deriving show]

and tele
  = TeleEmpty
  | TeleExtend of tele * tm_closure
[@@deriving show]

and ('a,'b) closure
  = Cl of env * 'a
  | ClFun of (eval_ctx -> t -> 'b)
  | ClConst of 'b
[@@deriving show]

and eval_ctx
  = { lvl   : lvl;
      env   : env;
      types : env;
    }
[@@deriving show]

and tm_closure = (Term.t, t) closure
[@@deriving show]

let env_lookup env (Idx(n) : idx) =
  let rec go env i = match env, i with
  | EEmpty, _       -> failwith ("impossible " ^ __LOC__)
  | ESnoc(_  ,a), 0 -> a
  | ESnoc(env,_), n -> go env (n-1)
  in go env n

let var x = Var(x,RSEmpty,SEmpty)
let lam ty f = Lam(ty,ClFun(f))
let sigma a b = Sigma(a,ClFun(b))
let pi a b = Pi(a,ClFun(b))
let arr a b = Pi(a,ClConst(b))

let rec id_env lvl : env =
  match lvl with
  | 0 -> EEmpty
  | n -> ESnoc(id_env (lvl-1), var (lvl-1))


%{

    %}


%token EOF

%token LPAREN "("
%token RPAREN ")"
%token LBRACE "{"
%token RBRACE "}"
%token LBRACK "["
%token RBRACK "]"
%token BACKSLASH "\\"
%token UNDERSCORE "_"
%token EQUAL "="
%token COMMA ","
%token COLON ":"
%token SEMICOLON ";"
%token DOT "."
%token RARROW "->"
%token STAR "*"

%token DEF
%token LET
%token TYPE
%token FST
%token SND
%token UNIT
%token UNIT_TT
%token BOTTOM
%token ABSURD
%token BOOL
%token TRUE
%token FALSE
%token ITE
%token AP

%token <string> IDENT

%start <Raw.top list> file

%%

file:
  | xs=list(terminated(top, ";")); EOF { xs }

top:
  | DEF; x=ident_opt; "="; t=tm; { Raw.Def(x,None,t) }
  | DEF; x=ident_opt; ":"; a=tm; "="; t=tm; { Raw.Def(x,Some(a),t) }

(* *)

tm_atom:
  | "("; x=tm; ")" { x }
  | x=IDENT { Raw.Var(x) }
  | TYPE { Raw.Type }
  | UNIT { Raw.Unit }
  | UNIT_TT { Raw.UnitTT }
  | BOTTOM { Raw.Bottom }
  | ABSURD { Raw.Absurd }
  | BOOL { Raw.Bool }
  | TRUE { Raw.True }
  | FALSE { Raw.False }
  | ITE { Raw.ITE }
  | x=tm_ap { x }

tm_spine_item:
  | x=tm_atom { fun a -> Raw.App(a,x) }
  | FST { fun a -> Raw.Fst(a) }
  | SND { fun a -> Raw.Snd(a) }

tm_spine:
  | ys=list(tm_spine_item) { ys }

tm_apps:
  | x=tm_atom; ys=tm_spine { List.fold_left (fun a b -> b a) x ys }

tm_arrs:
  | x=tm_apps { x }
  | x=tm_apps; "->"; y=tm_arrs { Raw.Pi((None,Some(x)), y) }
  | xs=nonempty_list(binder_full); "->"; y=tm_arrs { List.fold_right (fun a b -> Raw.Pi(a,b)) xs y }
  | "("; x=ident_opt; ":"; a=tm; ")"; "*"; y=tm_arrs { Raw.Sigma(x,a,y) }
  | x=tm_apps; "*"; y=tm_arrs { Raw.Sigma(None,x,y) }

tm_pairs:
  | x=tm_arrs; ","; y=tm_pairs { Raw.Pair(x,y) }
  | x=tm_arrs { x }

tm_let:
  | LET; x=IDENT; "="; t=tm; ";"; u=tm { Raw.Let(x,None,t,u) }
  | LET; x=IDENT; ":"; a=tm; "="; t=tm; ";"; u=tm { Raw.Let(x,Some(a),t,u) }

binder:
  | x=ident_opt { (x,None) }
  | y=binder_full { y }

binder_full:
  | "("; x=ident_opt; ":"; a=tm ")" { (x,Some(a)) }

tm_lambda:
  | "\\"; xs=list(binder); "."; u=tm
    { List.fold_right (fun a b -> Raw.Lam(a,b)) xs u }

tm_binder:
  | x=tm_lambda { x }

tele_item:
  | x=ident_opt; ":"; a=tm { (x,a) }

tele:
  | { Raw.TeleEmpty }
  | xs=separated_nonempty_list(";", tele_item)
    { List.fold_left (fun t (x,a) -> Raw.TeleExtend(t,x,a)) Raw.TeleEmpty xs }

tm_ap:
  | AP; "["; x=tele; "]"; y=tm_atom; z=tm_atom { Raw.Ap(x,y,z) }

tm:
  | x=tm_binder { x }
  | x=tm_let { x }
  | x=tm_pairs { x }

ident_opt:
  | x=IDENT { Some(x) }
  | "_" { None }

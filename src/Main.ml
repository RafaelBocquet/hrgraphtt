open Lexer
open Common
open Value
open Meta
open Top

let parse inchan =
  let lb     = Sedlexing.Utf8.from_channel inchan in
  let token  = Sedlexing.with_tokenizer Lexer.token lb in
  let result = MenhirLib.Convert.Simplified.traditional2revised Parser.file token in
  result

let parse_stdin () = parse stdin

let _ =
  Format.set_margin 140;
  let f = parse_stdin () in
  Format.fprintf Format.std_formatter "%a@." Raw.pp_file f;
  let t1 = Sys.time() in
  Elab.elab_file f;
  let t2 = Sys.time() in
  Format.fprintf Format.std_formatter "OK@.";
  Format.fprintf Format.std_formatter "Elapsed: %f@." (t2 -. t1);
  ()

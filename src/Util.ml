let pp_vector pp fmt x =
  Format.fprintf fmt "[";
  for i = 0 to CCVector.size x - 1 do
    if i <> 0 then Format.fprintf fmt "; ";
    pp fmt (CCVector.get x i)
  done;
  Format.fprintf fmt "]"


open Sedlexing
open Parser

let digit  = [%sedlex.regexp? '0' .. '9']
let int    = [%sedlex.regexp? Star digit]

let ident = [%sedlex.regexp? id_start, (Star id_continue)]
let operator = [%sedlex.regexp? Plus (sc | sm | so)]

let rec token lb =
  match%sedlex lb with
  | eof -> EOF
  | white_space -> token lb
  | "--", Star (Compl ('\n' | '\r')) -> token lb

  | '('  -> LPAREN
  | ')'  -> RPAREN
  | '{'  -> LBRACE
  | '}'  -> RBRACE
  | '['  -> LBRACK
  | ']'  -> RBRACK
  | '\\' -> BACKSLASH
  | '_'  -> UNDERSCORE
  | '='  -> EQUAL
  | ','  -> COMMA
  | ':'  -> COLON
  | ';'  -> SEMICOLON
  | '.'  -> DOT
  | "->" -> RARROW
  | "*"  -> STAR

  | "def"    -> DEF
  | "let"    -> LET
  | "Type"   -> TYPE
  | "Unit"   -> UNIT
  | "tt"     -> UNIT_TT
  | "Bottom" -> BOTTOM
  | "absurd" -> ABSURD
  | "Bool"   -> BOOL
  | "true"   -> TRUE
  | "false"  -> FALSE
  | "ite"    -> ITE
  | "ap"     -> AP

  | ".1" -> FST
  | ".2" -> SND
  | ident -> IDENT (Utf8.lexeme lb)

  | _ -> failwith __LOC__

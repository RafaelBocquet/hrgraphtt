open Common

type name_data
  = Local of Value.t * Value.t
  | Global of top

type ctx = { gctx  : Global.ctx;
             lctx  : EvalCtx.ctx;

             names : name_data NameMap.t;
             env   : Value.env;
           }

let empty gctx globals : ctx
  = { gctx  = gctx;
      lctx  = EvalCtx.empty;

      names = globals;
      env   = Value.EEmpty;
    }

let bind (ctx : ctx) x vty : ctx
  = { gctx  = ctx.gctx;
      lctx  = EvalCtx.bind ctx.lctx vty;

      names = begin match x with
        | Some(x) -> NameMap.add x (Local(Value.var ctx.lctx.lvl, vty)) ctx.names
        | None -> ctx.names
      end;
      env   = Value.ESnoc(ctx.env, Value.var ctx.lctx.lvl);
    }

let bind_tele (ctx : ctx) (rtele : Raw.tele) (vtele : Value.tele) : ctx =
  let vty = Eval.tele_type vtele in
  let rec go_names names v : Raw.tele * Value.tele -> _ = function
    | TeleEmpty, TeleEmpty ->
      names
    | TeleExtend(rtele,x_opt,_), TeleExtend(vtele,a) -> begin
      let names = go_names names (Eval.fst v) (rtele,vtele) in
      match x_opt with
      | Some(x) ->
        NameMap.add x (Local(Eval.snd v, Eval.clApp ctx.gctx ctx.lctx a (Eval.fst v))) names
      | None ->
        names
    end
    | _ -> failwith ("impossible " ^ __LOC__)
  in
  { gctx  = ctx.gctx;
    lctx  = EvalCtx.bind ctx.lctx vty;

    names = go_names ctx.names (Value.var ctx.lctx.lvl) (rtele,vtele);
    env   = Value.ESnoc(ctx.env, Value.var ctx.lctx.lvl);
  }

let define (ctx : ctx) x vty vu : ctx
  = { gctx  = ctx.gctx;
      lctx  = EvalCtx.bind ctx.lctx vty;
      names = begin match x with
        | Some x -> NameMap.add x (Local(Value.var ctx.lctx.lvl, vty)) ctx.names
        | None -> ctx.names
      end;
      env   = Value.ESnoc(ctx.env, vu);
    }

let lookup ctx x : Term.t * Value.t
  = try match NameMap.find x ctx.names with
  | Local(v,vty) ->
    Eval.quote ctx.gctx ctx.lctx v, vty
  | Global(i) -> Term.Top(i), (Top.lookup ctx.gctx.tctx i).vty
  with
  | Not_found -> failwith ("Not_found " ^ x)

let eval ctx = Eval.eval ctx.gctx ctx.lctx ctx.env
let eval_tele ctx = Eval.eval_tele ctx.gctx ctx.lctx ctx.env
let quote ctx = Eval.quote ctx.gctx ctx.lctx

open Common
module GCtx = Global
module LCtx = EvalCtx

exception ConvError

let rec eval (gctx : GCtx.ctx) (ctx : LCtx.ctx) (env : Value.env) : Term.t -> Value.t = function
  | Var(i)  ->
    Value.env_lookup env i
  | Top(i)  ->
    (Top.lookup gctx.tctx i).vtm
  | Let(t,u) ->
    let vt = eval gctx ctx env t in
    eval gctx ctx (ESnoc(env,vt)) u
  | Type -> Type
  (* Pi *)
  | Pi(a,b) ->
    let va = eval gctx ctx env a in
    Value.Pi(va, Cl(env,b))
  | Lam(ty,b)  ->
    Value.Lam(eval gctx ctx env ty, Cl(env,b))
  | App(f,ty,a) ->
    app gctx ctx (eval gctx ctx env f) (eval gctx ctx env ty) (eval gctx ctx env a)
  (* Sigma *)
  | Sigma(a,b) ->
    let va = eval gctx ctx env a in
    Value.Sigma(va, Cl(env,b))
  | Pair(a,b) ->
    Value.Pair(eval gctx ctx env a, eval gctx ctx env b)
  | Fst(p) ->
    fst (eval gctx ctx env p)
  | Snd(p) ->
    snd (eval gctx ctx env p)
  (* Unit *)
  | Unit ->
    Unit
  | UnitTT ->
    UnitTT
  (* Bottom *)
  | Bottom ->
    Bottom
  | Absurd(p,x) ->
    absurd (Value.Cl(env,p)) (eval gctx ctx env x)
  (* Bool *)
  | Bool ->
    Bool
  | True ->
    True
  | False ->
    False
  | ITE(p,t,f,b) ->
    ite (Value.Cl(env,p)) (eval gctx ctx env t) (eval gctx ctx env f) (eval gctx ctx env b)

  | Ap0(tele,ty,tm) ->
    let vtele   = eval_tele gctx ctx env tele in
    let tele_ty = tele_type vtele in
    Value.(
      lam tele_ty (fun ctx x0 ->
          lam tele_ty (fun ctx x1 ->
              lam (id_tele gctx ctx vtele x0 x1) (fun ctx x2 ->
                  ap gctx ctx vtele (Value.Cl(env,ty)) (Value.Cl(env,tm))
                    x0 x1 x2))))

  | Ap(tele,ty,tm,x0,x1,x2) ->
    ap gctx ctx (eval_tele gctx ctx env tele) (Value.Cl(env,ty)) (Value.Cl(env,tm))
      (eval gctx ctx env x0) (eval gctx ctx env x1) (eval gctx ctx env x2)

(* | t -> *)
(*   Format.fprintf Format.std_formatter "TODO: %a\n" Term.pp t; *)
(*   failwith __LOC__ *)

and appSp gctx ctx t : Value.spine -> Value.t = function
  | SEmpty -> t
  | SApp(sp,ty,a) -> app gctx ctx (appSp gctx ctx t sp) ty a
  | SFst(sp) -> fst (appSp gctx ctx t sp)
  | SSnd(sp) -> snd (appSp gctx ctx t sp)
  | SAbsurd(p,sp) -> absurd p (appSp gctx ctx t sp)
  | SITE(p,xt,xf,sp) -> ite p xt xf (appSp gctx ctx t sp)

and clApp (gctx : GCtx.ctx) ctx (cl : Value.tm_closure) (v : Value.t) : Value.t
  = match cl with
  | Cl(env,tm) -> eval gctx ctx (ESnoc(env,v)) tm
  | ClFun(f) -> f ctx v
  | ClConst(v) -> v

and appVar (gctx : GCtx.ctx) ctx f ty x : Value.t
  = app gctx ctx f ty (Value.var x)

and clAppVar (gctx : GCtx.ctx) ctx (cl : Value.tm_closure) x : Value.t
  = clApp gctx ctx cl (Value.var x)

and app gctx ctx (f : Value.t) ty (a : Value.t) = match f with
  | Var(i,rsp,sp) -> Var(i,rsp,SApp(sp,ty,a))
  | Lam(_,cl) -> clApp gctx ctx cl a
  | _ -> failwith ("impossible: " ^ __LOC__)

and fst : Value.t -> Value.t = function
  | Var(i,rsp,sp) -> Var(i,rsp,SFst(sp))
  | Pair(x,_) -> x
  | _ -> failwith ("impossible: " ^ __LOC__)

and snd : Value.t -> Value.t = function
  | Var(i,rsp,sp) -> Var(i,rsp,SSnd(sp))
  | Pair(_,y) -> y
  | _ -> failwith ("impossible: " ^ __LOC__)

and absurd p : Value.t -> Value.t = function
  | Var(i,rsp,sp) -> Var(i,rsp,SAbsurd(p,sp))
  | _ -> failwith ("impossible: " ^ __LOC__)

and ite p t f : Value.t -> Value.t = function
  | Var(i,rsp,sp) -> Var(i,rsp,SITE(p,t,f,sp))
  | True -> t
  | False -> f
  | _ -> failwith ("impossible: " ^ __LOC__)

and eval_tele gctx ctx (env : Value.env) : Term.tele -> Value.tele = function
  | TeleEmpty -> TeleEmpty
  | TeleExtend(a,b) -> TeleExtend(eval_tele gctx ctx env a, Cl(env,b))

and tele_type : Value.tele -> Value.t = function
  | TeleEmpty -> Unit
  | TeleExtend(a,b) -> Sigma(tele_type a,b)

and sigma_first = function
  | Value.Sigma(b,_) -> b
  | _ -> failwith ("impossible " ^ __LOC__)

and sigma_second = function
  | Value.Sigma(_,c) -> c
  | _ -> failwith ("impossible " ^ __LOC__)

and pi_first = function
  | Value.Pi(b,_) -> b
  | _ -> failwith ("impossible " ^ __LOC__)

and pi_second = function
  | Value.Pi(_,c) -> c
  | _ -> failwith ("impossible " ^ __LOC__)

and lam_body = function
  | Value.Lam(_,c) -> c
  | _ -> failwith ("impossible " ^ __LOC__)

and id_tele gctx ctx tele x0 x1
  = id gctx ctx Value.TeleEmpty (Value.ClConst(tele_type tele))
    Value.UnitTT Value.UnitTT Value.UnitTT x0 x1

and id gctx ctx tele a x0 x1 x2 y0 y1 =
  let e   = ap gctx ctx tele (Value.ClConst(Value.Type)) a x0 x1 x2 in
  let ax0 = clApp gctx ctx a x0 in
  let ax1 = clApp gctx ctx a x1 in
  let r = app gctx ctx (app gctx ctx e ax0 y0) ax1 y1 in
  r

and ap gctx ctx tele ty tm x0 x1 x2 =
  let tele_ty = tele_type tele in
  let vtm = clAppVar gctx (LCtx.bind ctx tele_ty) tm ctx.lvl in
  match vtm with
  (* Neutrals *)

  | Var(i,rsp,sp) ->
    let rec go_spine : Value.spine -> _ = function
      | SEmpty when i = ctx.lvl -> x2
      | SEmpty -> Var(i,RSRefl(rsp),SEmpty)
      | SApp(sp, ty, a) ->
        let open Value in
        let t     = go_spine sp in
        let qty   = quote gctx (LCtx.bind ctx tele_ty) ty in
        let qa    = quote gctx (LCtx.bind ctx tele_ty) a in
        let clty  = ClFun(fun ctx2 x -> eval gctx ctx2 (ESnoc(ctx.env, x)) qty) in
        let cla   = ClFun(fun ctx2 x -> eval gctx ctx2 (ESnoc(ctx.env, x)) qa) in
        let fty x = clApp gctx ctx clty x in
        let fa x  = clApp gctx ctx cla x in
        let tyx0  = fty x0 in
        let tyx1  = fty x1 in
        let ax0   = fa x0 in
        let ax1   = fa x1 in
        let tyx2  = id gctx ctx tele clty x0 x1 x2 ax0 ax1 in
        let ax2   = ap gctx ctx tele clty cla x0 x1 x2 in
        app gctx ctx
          (app gctx ctx
             (app gctx ctx t
                tyx0 ax0)
             tyx1 ax1)
          tyx2 ax2
      | SFst(sp) ->
        fst(go_spine sp)
      | SSnd(sp) ->
        snd(go_spine sp)
      | SAbsurd(p,sp) ->
        failwith "TODO"
      | SITE(p,xt,xf,sp) ->
        failwith "TODO"
    in go_spine sp

  (* Types *)

  | Type ->
    Value.(
      lam Type (fun ctx a -> lam Type (fun ctx b -> rel a b)))

  | Unit ->
    Value.(
      lam Unit (fun ctx _ -> lam Unit (fun ctx _ -> Unit)))

  | Sigma(_) ->
    let open Value in
    let clb = ClFun(fun ctx x -> sigma_first (clApp gctx ctx tm x)) in
    let idb ctx y0 y1 = id gctx ctx tele clb x0 x1 x2 y0 y1 in
    let clc = ClFun(fun ctx x -> clApp gctx ctx (sigma_second (clApp gctx ctx tm (fst x))) (snd x)) in
    let idc ctx y0 y1 y2 z0 z1 = id gctx ctx (TeleExtend(tele,clb)) clc (Pair(x0,y0)) (Pair(x1,y1)) (Pair(x2,y2)) z0 z1 in
    let tyx0 = clApp gctx ctx ty x0 in
    let tyx1 = clApp gctx ctx ty x1 in
    Value.(
      lam tyx0 (fun ctx p0 -> lam tyx1 (fun ctx p1 ->
          let y0 = fst p0 in
          let y1 = fst p1 in
          let z0 = snd p0 in
          let z1 = snd p1 in
          sigma (idb ctx y0 y1) (fun ctx y2 -> idc ctx y0 y1 y2 z0 z1)
        )))

  | Pi(_) ->
    let open Value in
    let clb
      = ClFun(fun ctx x -> pi_first (clApp gctx ctx tm x)) in
    let idb ctx y0 y1
      = id gctx ctx tele clb x0 x1 x2 y0 y1 in
    let clc
      = ClFun(fun ctx x -> clApp gctx ctx (pi_second (clApp gctx ctx tm (fst x))) (snd x)) in
    let idc ctx y0 y1 y2 z0 z1
      = id gctx ctx (TeleExtend(tele,clb)) clc (Pair(x0,y0)) (Pair(x1,y1)) (Pair(x2,y2)) z0 z1 in
    let bx0 = clApp gctx ctx clb x0 in
    let bx1 = clApp gctx ctx clb x1 in
    let tyx0 = clApp gctx ctx ty x0 in
    let tyx1 = clApp gctx ctx ty x1 in
    lam tyx0 (fun ctx f0 ->
        lam tyx1 (fun ctx f1 ->
            pi bx0 (fun ctx y0 ->
                pi bx1 (fun ctx y1 ->
                    pi (idb ctx y0 y1) (fun ctx y2 ->
                        idc ctx y0 y1 y2 (app gctx ctx f0 bx0 y0) (app gctx ctx f1 bx1 y1)
                      )))))

  (* Terms *)

  | UnitTT ->
    UnitTT

  | Pair(_) ->
    let open Value in
    let clb = ClFun(fun ctx x -> sigma_first (clApp gctx ctx ty x)) in
    let clc = ClFun(fun ctx x ->
        clApp gctx ctx (sigma_second (clApp gctx ctx ty x))
          (fst (clApp gctx ctx tm x))) in
    let clfst = ClFun(fun ctx x -> fst (clApp gctx ctx tm x)) in
    let clsnd = ClFun(fun ctx x -> snd (clApp gctx ctx tm x)) in
    Pair(
      ap gctx ctx tele clb clfst x0 x1 x2,
      ap gctx ctx tele clc clsnd x0 x1 x2)

  | Lam(_) ->
    let open Value in
    let clb
      = ClFun(fun ctx x -> pi_first (clApp gctx ctx ty x)) in
    let idb ctx y0 y1
      = id gctx ctx tele clb x0 x1 x2 y0 y1 in
    let clc
      = ClFun(fun ctx x -> clApp gctx ctx (pi_second (clApp gctx ctx ty (fst x))) (snd x)) in
    let clbody
      = ClFun(fun ctx x -> clApp gctx ctx (lam_body (clApp gctx ctx ty (fst x))) (snd x)) in
    let apc ctx y0 y1 y2
      = ap gctx ctx (TeleExtend(tele,clb)) clc clbody (Pair(x0,y0)) (Pair(x1,y1)) (Pair(x2,y2)) in
    let bx0 = pi_first (clApp gctx ctx ty x0) in
    let bx1 = pi_first (clApp gctx ctx ty x1) in
    lam bx0 (fun ctx y0 ->
        lam bx1 (fun ctx y1 ->
            lam (idb ctx y0 y1) (fun ctx y2 ->
                apc ctx y0 y1 y2)))

  | v ->
    Format.fprintf Format.std_formatter "AP: %a@." Value.pp v;
    failwith "TODO"

and rel a b
  = Value.(arr a (arr b Type))

and conv_bool gctx ctx (ty : Value.t) (a : Value.t) (b : Value.t) =
  try conv gctx ctx ty a b; true
  with ConvError -> false

and conv gctx ctx (ty : Value.t) (a : Value.t) (b : Value.t) : unit =
  conv_ gctx ctx (ty,a,b)

and conv_ gctx ctx : Value.t * Value.t * Value.t -> unit = function
  (* Pi *)
  | _,Pi(a1,b1),Pi(a2,b2) ->
    conv gctx ctx Type a1 a2;
    conv gctx (LCtx.bind ctx a1) Type
      (clAppVar gctx (LCtx.bind ctx a1) b1 ctx.lvl)
      (clAppVar gctx (LCtx.bind ctx a1) b2 ctx.lvl)
  | Pi(a,b),t1,t2 ->
    conv gctx (LCtx.bind ctx a)
      (clAppVar gctx (LCtx.bind ctx a) b ctx.lvl)
      (appVar gctx (LCtx.bind ctx a) t1 a ctx.lvl)
      (appVar gctx (LCtx.bind ctx a) t2 a ctx.lvl)
  (* Sigma *)
  | _,Sigma(a1,b1),Sigma(a2,b2) ->
    conv gctx ctx Type a1 a2;
    conv gctx (LCtx.bind ctx a1) Type
      (clAppVar gctx (LCtx.bind ctx a1) b1 ctx.lvl)
      (clAppVar gctx (LCtx.bind ctx a1) b2 ctx.lvl)
  | Sigma(a,b),p1,p2 ->
    let x1 = fst p1 in
    conv gctx ctx a x1 (fst p2);
    conv gctx ctx (clApp gctx ctx b x1) (snd p1) (snd p2)
  (* Unit *)
  | _,Unit,Unit -> ()
  | Unit,_,_ -> ()
  (* Bottom *)
  | _,Bottom,Bottom -> ()
  | Bottom,_,_ -> ()
  (* Bool *)
  | _,Bool,Bool -> ()
  | _,True,True -> ()
  | _,False,False -> ()
  (* Other *)
  | _,Var(x1,rsp1,sp1),Var(x2,rsp2,sp2) when x1 = x2 && rsp1 = rsp2 ->
    conv_spine gctx ctx (sp1,sp2)
  | _,Type,Type -> ()

  | _ ->
    raise ConvError

and conv_spine gctx ctx : Value.spine * Value.spine -> unit = function
  | SEmpty,SEmpty -> ()
  | SApp(sp1,ty,a1),SApp(sp2,_,a2) ->
    conv_spine gctx ctx (sp1,sp2);
    conv gctx ctx ty a1 a2
  | SFst(sp1),SFst(sp2) ->
    conv_spine gctx ctx (sp1,sp2)
  | SSnd(sp1),SSnd(sp2) ->
    conv_spine gctx ctx (sp1,sp2)
  | SAbsurd(p1,sp1), SAbsurd(p2,sp2) ->
    conv gctx (LCtx.bind ctx Bottom) Type
      (clAppVar gctx (LCtx.bind ctx Bottom) p1 ctx.lvl)
      (clAppVar gctx (LCtx.bind ctx Bottom) p2 ctx.lvl);
    conv_spine gctx ctx (sp1, sp2)
  | SITE(p1,t1,f1,sp1), SITE(p2,t2,f2,sp2) ->
    conv gctx (LCtx.bind ctx Bool) Type
      (clAppVar gctx (LCtx.bind ctx Bool) p1 ctx.lvl)
      (clAppVar gctx (LCtx.bind ctx Bool) p2 ctx.lvl);
    conv gctx ctx (clApp gctx ctx p1 True) t1 t2;
    conv gctx ctx (clApp gctx ctx p1 False) f1 f2;
    conv_spine gctx ctx (sp1, sp2)

  | _ ->
    raise ConvError

and quote (gctx : GCtx.ctx) ctx : Value.t -> Term.t = function
  | Var(v,rsp,sp) ->
    let _,t = quote_refl_spine gctx ctx v rsp in
    quote_spine gctx ctx t sp
  | Type -> Type

  (* Pi *)
  | Pi(a,b) ->
    Pi(quote gctx ctx a,
       quote gctx (LCtx.bind ctx a) (clAppVar gctx (LCtx.bind ctx a) b ctx.lvl))
  | Lam(ty,b) ->
    Lam(quote gctx ctx ty,
        quote gctx (LCtx.bind ctx ty) (clAppVar gctx (LCtx.bind ctx ty) b ctx.lvl))

  (* Sigma *)
  | Sigma(a,b) ->
    Sigma(quote gctx ctx a,
          quote gctx (LCtx.bind ctx a) (clAppVar gctx (LCtx.bind ctx a) b ctx.lvl))
  | Pair(x,y) ->
    Pair(quote gctx ctx x,
         quote gctx ctx y)

  (* Unit *)
  | Unit ->
    Unit
  | UnitTT ->
    UnitTT

  (* Bottom *)
  | Bottom ->
    Bottom

  (* Bool *)
  | Bool ->
    Bool
  | True ->
    True
  | False ->
    False

(* | v -> *)
(*   Format.fprintf Format.std_formatter "%a\n" Value.pp v; *)
(*   failwith __LOC__ *)

and quote_refl_spine gctx ctx i : Value.refl_spine -> Term.t * Term.t = function
  | RSEmpty ->
    let j = lvl_to_idx ctx.lvl i in
    quote gctx ctx (Value.env_lookup ctx.types j),
    Term.Var(j)
  | RSRefl(rsp) ->
    let ty, t = quote_refl_spine gctx (LCtx.bind ctx Unit) i rsp in
    Term.(Ap(TeleEmpty, Type, ty, UnitTT, UnitTT, UnitTT),
          Ap(TeleEmpty, ty, t, UnitTT, UnitTT, UnitTT))

and quote_spine gctx ctx t : Value.spine -> Term.t = function
  | SEmpty -> t
  | SApp(sp,ty,v) ->
    App(quote_spine gctx ctx t sp, quote gctx ctx ty, quote gctx ctx v)
  | SFst(sp) ->
    Fst(quote_spine gctx ctx t sp)
  | SSnd(sp) ->
    Snd(quote_spine gctx ctx t sp)
  | SAbsurd(p,sp) ->
    Absurd(quote gctx (LCtx.bind ctx Bottom) (clAppVar gctx (LCtx.bind ctx Bottom) p ctx.lvl),
           quote_spine gctx ctx t sp)
  | SITE(p,xt,xf,sp) ->
    ITE(quote gctx (LCtx.bind ctx Bool) (clAppVar gctx (LCtx.bind ctx Bool) p ctx.lvl),
        quote gctx ctx xt,
        quote gctx ctx xf,
        quote_spine gctx ctx t sp)

and quote_tele gctx ctx : Value.tele -> Term.tele = function
  | TeleEmpty ->
    TeleEmpty
  | TeleExtend(a,b) ->
    TeleExtend(quote_tele gctx ctx a,
               quote gctx (LCtx.bind ctx (tele_type a)) (clAppVar gctx (LCtx.bind ctx (tele_type a)) b ctx.lvl))
